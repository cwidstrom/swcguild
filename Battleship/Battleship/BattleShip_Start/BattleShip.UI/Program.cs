﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;


namespace BattleShip.UI
{
    class Program
    {
        static TitleScreen welcomeMessage = new TitleScreen();
        static Players p1 = new Players();
        static Players p2 = new Players();
        static ValueSetting board1 = new ValueSetting();

        static void Main(string[] args)
        {
            

            welcomeMessage.introMessage();
            p1.gameStart();
            

        }
    }

}