﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.UI
{
    public class ValueSetting
    {
        public int letterConverter(string letter)
        {
            if (letter == "a" || letter ==  "A")
            {
                return 1;
            }
            else if (letter == "b" || letter == "B")
            {
                return 2;
            }
            else if (letter == "c" || letter == "C")
            {
                return 3;
            }
            else if (letter == "d" || letter == "D")
            {
                return 4;
            }
            else if (letter == "e" || letter == "E")
            {
                return 5;
            }
            else if (letter == "f" || letter == "F")
            {
                return 6;
            }
            else if (letter == "g" || letter == "G")
            {
                return 7;
            }
            else if (letter == "h" || letter == "H")
            {
                return 8;
            }
            else if (letter == "i" || letter == "I")
            {
                return 9;
            }
            else if (letter == "j" || letter == "J")
            {
                return 10;
            }
            else
            {
                return 15;
            }
        }

        public void clearBoard()
        {
            int rowLength = autoTable.GetLength(0);

            int colLength = autoTable.GetLength(1);

            //its taking it as y,x


            for (int i = 1; i < rowLength; i++)
            {
                for (int j = 1; j < colLength; j++)
                {
                    autoTable[i, j] = "-";


                }
                Console.Write(Environment.NewLine);
            }



        }

        public void displayBoard(Board board)
        {
            int rowLength = autoTable.GetLength(0);

            int colLength = autoTable.GetLength(1);

            //its taking it as y,x


            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    if (autoTable[j, i] == "M")
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write(string.Format("{0} ", autoTable[j, i]));
                        Console.ResetColor();
                    }
                    else if (autoTable[j, i] == "H" && i != 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(string.Format("{0} ", autoTable[j, i]));
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.Write(string.Format("{0} ", autoTable[j, i]));
                    }

                }
                Console.Write(Environment.NewLine);
            }

          

        }

        public string[,] autoTable = {
                {" ","1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "10" },
                {" A", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
                {"B", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
                {"C", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
                {"D", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
                {"E", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
                {"F", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
                {"G", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
                {"H", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
                {"I", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },
                {"J", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-" },

            };

              
                

                
            }
       
        
        }
    


