﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class TitleScreen
    {
        public void introMessage()
        {
            Console.WriteLine(@"
 ____        _   _   _         _____ _     _       
|  _ \      | | | | | |       / ____| |   (_)      
| |_) | __ _| |_| |_| | ___  | (___ | |__  _ _ __  
|  _ < / _` | __| __| |/ _ \  \___ \| '_ \| | '_ \ 
| |_) | (_| | |_| |_| |  __/  ____) | | | | | |_) |
|____/ \__,_|\__|\__|_|\___| |_____/|_| |_|_| .__/ 
                                            | |    
                                            |_|   
");
          
            Console.WriteLine("");
            Console.WriteLine("Welcome to BattleShip!");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }

        
            
    }
}
