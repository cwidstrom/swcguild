﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;


namespace BattleShip.UI
{
    class Players
    {
        ValueSetting board1 = new ValueSetting();
        ValueSetting board2 = new ValueSetting();
        static PlaceShipRequest destroyerRequest = new PlaceShipRequest();
        static PlaceShipRequest cruiserRequest = new PlaceShipRequest();
        static PlaceShipRequest submarineRequest = new PlaceShipRequest();
        static PlaceShipRequest battleshipRequest = new PlaceShipRequest();
        static PlaceShipRequest carrierRequest = new PlaceShipRequest();
        static int n;
       


        Board player1board = new Board();
        Board player2board = new Board();
            



        public void gameStart()
        {
            bool gameover = false;
            bool continueResponse = false;
            string response = "";
            var p1name = player1Name();
            var p2name = player2Name();
            do
            {
                player1ShipSelections(p1name);
                player2ShipSelections(p2name);
                Battle(p1name, p2name);
                do
                {
                    Console.WriteLine("Would you like to play again? (Y = Yes, N = No)");
                    response = Console.ReadLine();
                    if (response == "Y" || response == "y" || response == "N" || response == "n")
                    {
                        continueResponse = true;
                    }
                    else
                    {
                        continueResponse = false;
                    }
                } while (!continueResponse);

                if (response == "Y" || response == "y")
                {
                    board1.clearBoard();
                    board2.clearBoard();
                    
                    Board player1ClearBoard = new Board();
                    Board player2ClearBoard = new Board();
                    player1board = player1ClearBoard;
                    player2board = player2ClearBoard;
                    IsDead = false;
                    //player1board.ShotHistory = new Dictionary<Coordinate, ShotHistory>();
                    //player1board.resetIndex = 0;

                    //player2board.ShotHistory = new Dictionary<Coordinate, ShotHistory>();
                    //player2board.resetIndex = 0;
                    Console.Clear();
                    gameover = false;

                }
                else
                {
                    Console.WriteLine("Thanks for playing!");
                    Console.ReadLine();
                    gameover = true;
                }

            } while (!gameover);
        }

        public string player1Name()
        {
            Console.Write("Player 1, please enter your name: ");
            string inputName1 = Console.ReadLine();
            Console.Clear();
            return inputName1;
            

        }

        public void player1ShipSelections(string name1)
        {
            Console.WriteLine("");
            Console.WriteLine("{0}, steel yourself for war! Time to set up your board: ", name1);
            Console.WriteLine("");
            board1.displayBoard(player1board);
            Console.WriteLine("");
            Console.WriteLine("{0}, get ready to place your Destroyer", name1);
            //string destroyerCoord = "";

            bool correctinput = false;
            bool correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name1);
                var destroyerCoordinateInput = Console.ReadLine();

                if (destroyerCoordinateInput.Length < 2 || destroyerCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(destroyerCoordinateInput.Substring(1, destroyerCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(destroyerCoordinateInput.Substring(1, destroyerCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = destroyerCoordinateInput[0];
                    var letterToNumber = board1.letterConverter(destroyerCoordinateInput.Substring(0, 1));
                    var futureNumber = destroyerCoordinateInput.Substring(1, destroyerCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate destroyerCoordinate = new Coordinate(letterToNumber, makeNumber);
                    destroyerRequest.Coordinate = destroyerCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name1);
                        var destroyerDirection = Console.ReadLine();
                        if (destroyerDirection == "" || destroyerDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (destroyerDirection == "d" || destroyerDirection == "D")
                        {
                            destroyerRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (destroyerDirection == "u" || destroyerDirection == "U")
                        {
                            destroyerRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (destroyerDirection == "l" || destroyerDirection == "L")
                        {
                            destroyerRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (destroyerDirection == "r" || destroyerDirection == "R")
                        {
                            destroyerRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    destroyerRequest.ShipType = ShipType.Destroyer;
                    var destroyerResponse = player1board.PlaceShip(destroyerRequest);
                    if (ShipPlacement.NotEnoughSpace == destroyerResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == destroyerResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Destroyer placed!");


                        correctinput = true;

                    }

                }
            } while (!correctinput);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("{0}, get ready to place your Cruiser", name1);
            //string destroyerCoord = "";

            correctinput = false;
            correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name1);
                var cruiserCoordinateInput = Console.ReadLine();

                if (cruiserCoordinateInput.Length < 2 || cruiserCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(cruiserCoordinateInput.Substring(1, cruiserCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(cruiserCoordinateInput.Substring(1, cruiserCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = cruiserCoordinateInput[0];
                    var letterToNumber = board1.letterConverter(cruiserCoordinateInput.Substring(0, 1));
                    var futureNumber = cruiserCoordinateInput.Substring(1, cruiserCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate cruiserCoordinate = new Coordinate(letterToNumber, makeNumber);
                    cruiserRequest.Coordinate = cruiserCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name1);
                        var cruiserDirection = Console.ReadLine();
                        if (cruiserDirection == "" || cruiserDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (cruiserDirection == "d" || cruiserDirection == "D")
                        {
                            cruiserRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (cruiserDirection == "u" || cruiserDirection == "U")
                        {
                            cruiserRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (cruiserDirection == "l" || cruiserDirection == "L")
                        {
                            cruiserRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (cruiserDirection == "r" || cruiserDirection == "R")
                        {
                            cruiserRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    cruiserRequest.ShipType = ShipType.Cruiser;
                    var cruiserResponse = player1board.PlaceShip(cruiserRequest);
                    if (ShipPlacement.NotEnoughSpace == cruiserResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == cruiserResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Cruiser placed!");


                        correctinput = true;

                    }

                }
            } while (!correctinput);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("{0}, get ready to place your Submarine", name1);
            //string destroyerCoord = "";

            correctinput = false;
            correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name1);
                var submarineCoordinateInput = Console.ReadLine();

                if (submarineCoordinateInput.Length < 2 || submarineCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(submarineCoordinateInput.Substring(1, submarineCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(submarineCoordinateInput.Substring(1, submarineCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = submarineCoordinateInput[0];
                    var letterToNumber = board1.letterConverter(submarineCoordinateInput.Substring(0, 1));
                    var futureNumber = submarineCoordinateInput.Substring(1, submarineCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate submarineCoordinate = new Coordinate(letterToNumber, makeNumber);
                    submarineRequest.Coordinate = submarineCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name1);
                        var submarineDirection = Console.ReadLine();
                        if (submarineDirection == "" || submarineDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (submarineDirection == "d" || submarineDirection == "D")
                        {
                            submarineRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (submarineDirection == "u" || submarineDirection == "U")
                        {
                            submarineRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (submarineDirection == "l" || submarineDirection == "L")
                        {
                            submarineRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (submarineDirection == "r" || submarineDirection == "R")
                        {
                            submarineRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    submarineRequest.ShipType = ShipType.Submarine;
                    var submarineResponse = player1board.PlaceShip(submarineRequest);
                    if (ShipPlacement.NotEnoughSpace == submarineResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == submarineResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Submarine placed!");


                        correctinput = true;

                    }

                }
            } while (!correctinput);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("{0}, get ready to place your Battleship", name1);
            //string destroyerCoord = "";

            correctinput = false;
            correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name1);
                var battleshipCoordinateInput = Console.ReadLine();

                if (battleshipCoordinateInput.Length < 2 || battleshipCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(battleshipCoordinateInput.Substring(1, battleshipCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(battleshipCoordinateInput.Substring(1, battleshipCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = battleshipCoordinateInput[0];
                    var letterToNumber = board1.letterConverter(battleshipCoordinateInput.Substring(0, 1));
                    var futureNumber = battleshipCoordinateInput.Substring(1, battleshipCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate battleshipCoordinate = new Coordinate(letterToNumber, makeNumber);
                    battleshipRequest.Coordinate = battleshipCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name1);
                        var battleshipDirection = Console.ReadLine();
                        if (battleshipDirection == "" || battleshipDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (battleshipDirection == "d" || battleshipDirection == "D")
                        {
                            battleshipRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (battleshipDirection == "u" || battleshipDirection == "U")
                        {
                            battleshipRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (battleshipDirection == "l" || battleshipDirection == "L")
                        {
                            battleshipRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (battleshipDirection == "r" || battleshipDirection == "R")
                        {
                            battleshipRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    battleshipRequest.ShipType = ShipType.Battleship;
                    var battleshipResponse = player1board.PlaceShip(battleshipRequest);
                    if (ShipPlacement.NotEnoughSpace == battleshipResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == battleshipResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Battleship placed!");


                        correctinput = true;

                    }

                }
            } while (!correctinput);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("{0}, get ready to place your Carrier", name1);
            //string destroyerCoord = "";

            correctinput = false;
            correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name1);
                var carrierCoordinateInput = Console.ReadLine();

                if (carrierCoordinateInput.Length < 2 || carrierCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(carrierCoordinateInput.Substring(1, carrierCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(carrierCoordinateInput.Substring(1, carrierCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = carrierCoordinateInput[0];
                    var letterToNumber = board1.letterConverter(carrierCoordinateInput.Substring(0, 1));
                    var futureNumber = carrierCoordinateInput.Substring(1, carrierCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate carrierCoordinate = new Coordinate(letterToNumber, makeNumber);
                    carrierRequest.Coordinate = carrierCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name1);
                        var carrierDirection = Console.ReadLine();
                        if (carrierDirection == "" || carrierDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (carrierDirection == "d" || carrierDirection == "D")
                        {
                            carrierRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (carrierDirection == "u" || carrierDirection == "U")
                        {
                            carrierRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (carrierDirection == "l" || carrierDirection == "L")
                        {
                            carrierRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (carrierDirection == "r" || carrierDirection == "R")
                        {
                            carrierRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    carrierRequest.ShipType = ShipType.Carrier;
                    var carrierResponse = player1board.PlaceShip(carrierRequest);
                    if (ShipPlacement.NotEnoughSpace == carrierResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == carrierResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Carrier placed!");
                        Console.WriteLine("");
                        Console.WriteLine("Alright {0}, time to leave the room", name1);
                        Console.WriteLine("Press any key to continue");
                        Console.ReadLine();
                        Console.Clear();

                        correctinput = true;

                    }
                    
                }
            } while (!correctinput);

            
        }

        public string player2Name()
        {
            Console.Write("Player 2, please enter your name: ");
            string inputName2 = Console.ReadLine();
            Console.Clear();
            return inputName2;

        }

        public void player2ShipSelections(string name2)
        {
            //Console.Clear();
            //Console.WriteLine("Here comes a test....");

            //var coordinate = new Coordinate(10, 10);
            //var response = player1board.FireShot(coordinate);

            //Console.WriteLine(response.ShotStatus);

            Console.WriteLine("");
            Console.WriteLine("{0}, steel yourself for war! Time to set up your board: ", name2);
            Console.WriteLine("");
            board2.displayBoard(player2board);
            Console.WriteLine("");
            Console.WriteLine("{0}, get ready to place your Destroyer", name2);
            //string destroyerCoord = "";

            bool correctinput = false;
            bool correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name2);
                var destroyerCoordinateInput = Console.ReadLine();

                if (destroyerCoordinateInput.Length < 2 || destroyerCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(destroyerCoordinateInput.Substring(1, destroyerCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(destroyerCoordinateInput.Substring(1, destroyerCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = destroyerCoordinateInput[0];
                    var letterToNumber = board2.letterConverter(destroyerCoordinateInput.Substring(0, 1));
                    var futureNumber = destroyerCoordinateInput.Substring(1, destroyerCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate destroyerCoordinate = new Coordinate(letterToNumber, makeNumber);
                    destroyerRequest.Coordinate = destroyerCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name2);
                        var destroyerDirection = Console.ReadLine();
                        if (destroyerDirection == "" || destroyerDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (destroyerDirection == "d" || destroyerDirection == "D")
                        {
                            destroyerRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (destroyerDirection == "u" || destroyerDirection == "U")
                        {
                            destroyerRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (destroyerDirection == "l" || destroyerDirection == "L")
                        {
                            destroyerRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (destroyerDirection == "r" || destroyerDirection == "R")
                        {
                            destroyerRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    destroyerRequest.ShipType = ShipType.Destroyer;
                    var destroyerResponse = player2board.PlaceShip(destroyerRequest);
                    if (ShipPlacement.NotEnoughSpace == destroyerResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == destroyerResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Destroyer placed!");


                        correctinput = true;

                    }

                }
            } while (!correctinput);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("{0}, get ready to place your Cruiser", name2);
            //string destroyerCoord = "";

            correctinput = false;
            correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name2);
                var cruiserCoordinateInput = Console.ReadLine();

                if (cruiserCoordinateInput.Length < 2 || cruiserCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(cruiserCoordinateInput.Substring(1, cruiserCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(cruiserCoordinateInput.Substring(1, cruiserCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = cruiserCoordinateInput[0];
                    var letterToNumber = board2.letterConverter(cruiserCoordinateInput.Substring(0, 1));
                    var futureNumber = cruiserCoordinateInput.Substring(1, cruiserCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate cruiserCoordinate = new Coordinate(letterToNumber, makeNumber);
                    cruiserRequest.Coordinate = cruiserCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name2);
                        var cruiserDirection = Console.ReadLine();
                        if (cruiserDirection == "" || cruiserDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (cruiserDirection == "d" || cruiserDirection == "D")
                        {
                            cruiserRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (cruiserDirection == "u" || cruiserDirection == "U")
                        {
                            cruiserRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (cruiserDirection == "l" || cruiserDirection == "L")
                        {
                            cruiserRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (cruiserDirection == "r" || cruiserDirection == "R")
                        {
                            cruiserRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    cruiserRequest.ShipType = ShipType.Cruiser;
                    var cruiserResponse = player2board.PlaceShip(cruiserRequest);
                    if (ShipPlacement.NotEnoughSpace == cruiserResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == cruiserResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Cruiser placed!");


                        correctinput = true;

                    }

                }
            } while (!correctinput);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("{0}, get ready to place your Submarine", name2);
            //string destroyerCoord = "";
            
            correctinput = false;
            correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name2);
                var submarineCoordinateInput = Console.ReadLine();

                if (submarineCoordinateInput.Length < 2 || submarineCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(submarineCoordinateInput.Substring(1, submarineCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(submarineCoordinateInput.Substring(1, submarineCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = submarineCoordinateInput[0];
                    var letterToNumber = board2.letterConverter(submarineCoordinateInput.Substring(0, 1));
                    var futureNumber = submarineCoordinateInput.Substring(1, submarineCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate submarineCoordinate = new Coordinate(letterToNumber, makeNumber);
                    submarineRequest.Coordinate = submarineCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name2);
                        var submarineDirection = Console.ReadLine();
                        if (submarineDirection == "" || submarineDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (submarineDirection == "d" || submarineDirection == "D")
                        {
                            submarineRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (submarineDirection == "u" || submarineDirection == "U")
                        {
                            submarineRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (submarineDirection == "l" || submarineDirection == "L")
                        {
                            submarineRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (submarineDirection == "r" || submarineDirection == "R")
                        {
                            submarineRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    submarineRequest.ShipType = ShipType.Submarine;
                    var submarineResponse = player2board.PlaceShip(submarineRequest);
                    if (ShipPlacement.NotEnoughSpace == submarineResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == submarineResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Submarine placed!");


                        correctinput = true;

                    }

                }
            } while (!correctinput);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("{0}, get ready to place your Battleship", name2);
            //string destroyerCoord = "";
            
            correctinput = false;
            correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name2);
                var battleshipCoordinateInput = Console.ReadLine();

                if (battleshipCoordinateInput.Length < 2 || battleshipCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(battleshipCoordinateInput.Substring(1, battleshipCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(battleshipCoordinateInput.Substring(1, battleshipCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = battleshipCoordinateInput[0];
                    var letterToNumber = board2.letterConverter(battleshipCoordinateInput.Substring(0, 1));
                    var futureNumber = battleshipCoordinateInput.Substring(1, battleshipCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate battleshipCoordinate = new Coordinate(letterToNumber, makeNumber);
                    battleshipRequest.Coordinate = battleshipCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name2);
                        var battleshipDirection = Console.ReadLine();
                        if (battleshipDirection == "" || battleshipDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (battleshipDirection == "d" || battleshipDirection == "D")
                        {
                            battleshipRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (battleshipDirection == "u" || battleshipDirection == "U")
                        {
                            battleshipRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (battleshipDirection == "l" || battleshipDirection == "L")
                        {
                            battleshipRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (battleshipDirection == "r" || battleshipDirection == "R")
                        {
                            battleshipRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    battleshipRequest.ShipType = ShipType.Battleship;
                    var battleshipResponse = player2board.PlaceShip(battleshipRequest);
                    if (ShipPlacement.NotEnoughSpace == battleshipResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == battleshipResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Battleship placed!");


                        correctinput = true;

                    }

                }
            } while (!correctinput);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Console.WriteLine("{0}, get ready to place your Carrier", name2);
            //string destroyerCoord = "";
            
            correctinput = false;
            correctDirection = false;

            do
            {
                Console.Write("{0}, please enter a valid coordinate (example: A5): ", name2);
                var carrierCoordinateInput = Console.ReadLine();

                if (carrierCoordinateInput.Length < 2 || carrierCoordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters!");
                }
                else if (int.TryParse(carrierCoordinateInput.Substring(1, carrierCoordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else if (int.Parse(carrierCoordinateInput.Substring(1, carrierCoordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                }
                else
                {

                    var letter = carrierCoordinateInput[0];
                    var letterToNumber = board2.letterConverter(carrierCoordinateInput.Substring(0, 1));
                    var futureNumber = carrierCoordinateInput.Substring(1, carrierCoordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate carrierCoordinate = new Coordinate(letterToNumber, makeNumber);
                    carrierRequest.Coordinate = carrierCoordinate;
                    do
                    {
                        Console.Write("{0}, please enter a valid direction to place your ship (D-Down, U-Up, L-Left, R-Right) : ", name2);
                        var carrierDirection = Console.ReadLine();
                        if (carrierDirection == "" || carrierDirection.Length != 1)
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }

                        else if (carrierDirection == "d" || carrierDirection == "D")
                        {
                            carrierRequest.Direction = ShipDirection.Down;
                            correctDirection = true;
                        }
                        else if (carrierDirection == "u" || carrierDirection == "U")
                        {
                            carrierRequest.Direction = ShipDirection.Up;
                            correctDirection = true;
                        }
                        else if (carrierDirection == "l" || carrierDirection == "L")
                        {
                            carrierRequest.Direction = ShipDirection.Left;
                            correctDirection = true;
                        }
                        else if (carrierDirection == "r" || carrierDirection == "R")
                        {
                            carrierRequest.Direction = ShipDirection.Right;
                            correctDirection = true;
                        }
                        else
                        {
                            Console.WriteLine("That is not a valid input, please try again");
                        }


                    } while (!correctDirection);
                    carrierRequest.ShipType = ShipType.Carrier;
                    var carrierResponse = player2board.PlaceShip(carrierRequest);
                    if (ShipPlacement.NotEnoughSpace == carrierResponse)
                    {

                        Console.WriteLine("Not enough space, please re-enter the coordinates and direction");

                    }
                    else if (ShipPlacement.Overlap == carrierResponse)
                    {
                        Console.WriteLine("There's already a ship there, please re-enter the coordinates and direction");
                    }
                    else
                    {
                        Console.WriteLine("Carrier placed!");


                        correctinput = true;

                    }

                }
            } while (!correctinput);

            Console.Clear();
        }

        public bool IsDead = false;

        public void p1WarGames(string name1)
        {
            int n;
            bool correctinput = false;
            do
            {
                board2.displayBoard(player2board);
                Console.WriteLine("");
                Console.Write("{0}, please enter a valid coordinate to fire at (example: A5): ", name1);
                var coordinateInput = Console.ReadLine();

                if (coordinateInput.Length < 2 || coordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters, try again");
                    
                }
                else if (int.TryParse(coordinateInput.Substring(1, coordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                   
                }
                else if (int.Parse(coordinateInput.Substring(1, coordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                    
                }
                else
                {

                    var letter = coordinateInput[0];
                    var letterToNumber = board1.letterConverter(coordinateInput.Substring(0, 1));
                    var futureNumber = coordinateInput.Substring(1, coordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate shotCoordinate = new Coordinate(letterToNumber, makeNumber);
                    var shotRequest = player2board.FireShot(shotCoordinate);
                    if (shotRequest.ShotStatus == ShotStatus.Invalid)
                    {
                        Console.WriteLine("That was an invalid input, try again");
                        Console.ReadLine();
                        Console.Clear();
                    }
                    else if (shotRequest.ShotStatus == ShotStatus.Duplicate)
                    {
                        Console.WriteLine("You already fired at that coordinate, try again");
                        
                        Console.ReadLine();
                        Console.Clear();
                    }
                    else if (shotRequest.ShotStatus == ShotStatus.Miss)
                    {
                        Console.WriteLine("You missed!");

                        

                        board2.autoTable[letterToNumber, makeNumber] = "M";
                      
                        Console.ReadLine();
                        Console.Clear();
                        correctinput = true;
                    }
                    else if (shotRequest.ShotStatus == ShotStatus.Victory)
                    {
                        Console.WriteLine("You have sunk all of your oppontent's ships, you won!");
                        Console.ReadLine();
                        Console.Clear();
                        correctinput = true;
                        IsDead = true;

                    }
                    else if (shotRequest.ShotStatus == ShotStatus.HitAndSunk)
                    {
                        Console.WriteLine("Nice! You sunk their {0}", shotRequest.ShipImpacted);
                      
                        board2.autoTable[letterToNumber, makeNumber] = "H";
                       
                        Console.ReadLine();
                        Console.Clear();
                        correctinput = true;
                        //shotRequest.ShipImpacted
                        //response.ShipImpacted = ship.ShipName;
                    }
                    else
                    {
                        Console.WriteLine("You hit something!");
                    
                        board2.autoTable[letterToNumber, makeNumber] = "H";
                        
                        Console.ReadLine();
                        Console.Clear();
                        correctinput = true;
                    }
                }
            } while (!correctinput);

        }

        public void p2WarGames(string name2)
        {
            int n;
            
            bool correctinput = false;
            do
            {
                board1.displayBoard(player1board);
                Console.WriteLine("");
                Console.Write("{0}, please enter a valid coordinate to fire at (example: A5): ", name2);
                var coordinateInput = Console.ReadLine();

                if (coordinateInput.Length < 2 || coordinateInput.Length > 3)
                {
                    Console.WriteLine("You entered the wrong number of characters, try again");
                   
                }
                else if (int.TryParse(coordinateInput.Substring(1, coordinateInput.Length - 1), out n) == false)
                {
                    Console.WriteLine("Invalid input, try again");
                    
                }
                else if (int.Parse(coordinateInput.Substring(1, coordinateInput.Length - 1)) > 10)
                {
                    Console.WriteLine("Invalid input, try again");
                   
                }
                else
                {

                    var letter = coordinateInput[0];
                    var letterToNumber = board2.letterConverter(coordinateInput.Substring(0, 1));
                    var futureNumber = coordinateInput.Substring(1, coordinateInput.Length - 1);
                    int makeNumber = Convert.ToInt32(futureNumber);

                    Coordinate shotCoordinate = new Coordinate(letterToNumber, makeNumber);
                    var shotRequest = player1board.FireShot(shotCoordinate);
                    if (shotRequest.ShotStatus == ShotStatus.Invalid)
                    {
                        Console.WriteLine("That was an invalid coordinate, try again");
                        Console.ReadLine();
                        Console.Clear();
                    }
                    else if (shotRequest.ShotStatus == ShotStatus.Duplicate)
                    {
                        Console.WriteLine("You already fired at that coordinate, try again");
                        Console.ReadLine();
                        Console.Clear();
                    }
                    else if (shotRequest.ShotStatus == ShotStatus.Miss)
                    {
                        Console.WriteLine("You missed!");
                       
                        board1.autoTable[letterToNumber, makeNumber] = "M";
                        
                        Console.ReadLine();
                        Console.Clear();
                        correctinput = true;
                    }
                    else if (shotRequest.ShotStatus == ShotStatus.Victory)
                    {
                        Console.WriteLine("You sunk all of your opponent's ships, you won!");
                        Console.ReadLine();
                        Console.Clear();
                        correctinput = true;
                        IsDead = true;

                    }
                    else if (shotRequest.ShotStatus == ShotStatus.HitAndSunk)
                    {
                        Console.WriteLine("Nice! You sunk their {0}", shotRequest.ShipImpacted);
         
                        board1.autoTable[letterToNumber, makeNumber] = "H";
                        
                        Console.ReadLine();
                        Console.Clear();
                        correctinput = true;
                        //shotRequest.ShipImpacted
                        //response.ShipImpacted = ship.ShipName;
                    }
                    else
                    {
                        Console.WriteLine("You hit something!");
               
                        board1.autoTable[letterToNumber, makeNumber] = "H";
                        
                        Console.ReadLine();
                        Console.Clear();
                        correctinput = true;
                    }
                }
            } while (!correctinput);

        }


        public void Battle(string name1, string name2)
        {
            
            bool p1Turn = true;
            

            while (IsDead != true)
            {
                if (p1Turn)
                {
                    p1WarGames(name1);
                }
                else
                {
                    p2WarGames(name2);
                }

                p1Turn = !p1Turn;
            }

            Console.WriteLine("Game Over");
            
            



        }


    }
}